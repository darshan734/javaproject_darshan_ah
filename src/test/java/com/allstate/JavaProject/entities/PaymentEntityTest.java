package com.allstate.JavaProject.entities;

import com.allstate.JavaProject.dao.PaymentDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PaymentEntityTest {
    private Payment payment;

    @BeforeEach
    void setUp() {
        Date date = new Date();
        payment = new Payment(1,date, "Bank",1000.0, 100);
    }

    @Test
    void getId() {
        assertEquals(1, payment.getId());
    }

    @Test
    void getPaymentDate() {
        Date date = new Date();
        assertEquals(date, payment.getPaymentDate());
    }

    @Test
    void getType() {
        assertEquals("Bank", payment.getType());
    }

    @Test
    void getAmount() { assertEquals(1000.0, payment.getAmount());
    }

    @Test
    void getCustomerId() { assertEquals(100, payment.getCustomerId());
    }
}
