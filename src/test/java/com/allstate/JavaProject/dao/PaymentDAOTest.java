package com.allstate.JavaProject.dao;

import com.allstate.JavaProject.entities.Payment;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class PaymentDAOTest {

    @Autowired
    private PaymentDAO dao;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    public void save_And_findById_Success() {
        Date date = new Date();
        Payment payment = new Payment(4,date, "Bank", 20000.0, 400);
        dao.save(payment);
        assertEquals(payment.getId(), dao.findByID(4).getId());
        assertEquals(3, dao.findByType("Bank").size());
    }

    @AfterEach
    public  void cleanUp() {
        for (String collectionName : mongoTemplate.getCollectionNames()) {
            if (!collectionName.startsWith("system.")) {
                mongoTemplate.dropCollection(collectionName);
            }
        }
    }
}
