package com.allstate.JavaProject.dao;

import com.allstate.JavaProject.entities.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;

public class PaymentDAOMockTest {
    @Mock
    private PaymentDAO dao;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    public void rowCount_success() {
        doReturn(1).when(dao).rowcount();
        assertEquals(1, dao.rowcount());
    }

    @Test
    public void save_Success() {
        Payment payment = new Payment(2, new Date(), "Credit", 30000.0, 200);
        doReturn(0).when(dao).save(any(Payment.class));
        assertEquals(0, dao.save(payment));
    }

    @Test
    public void findByID_Success() {
        Payment payment = new Payment(1, new Date(), "Bank", 1000.00, 300);
        doReturn(payment).when(dao).findByID(1);
        assertEquals(payment, dao.findByID(1));
    }

    @Test
    public void findByType_Success() {
        List<Payment> payments = new ArrayList<Payment>(Arrays.asList(
                new Payment(1, new Date(), "Bank", 1000.0, 500),
                new Payment(4, new Date(), "bANK", 2000.0, 6000)
        ));
        doReturn(payments).when(dao).findByType("Bank");
        assertEquals(payments, dao.findByType("Bank"));
    }
}
