package com.allstate.JavaProject.rest;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import static org.junit.jupiter.api.Assertions.*;

public class PaymentControllerTest {
    int port=8080;

    @Test
    public void paymentShouldReturnDefaultMessage() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:" + port
                + "/api/payments/apiStatus", String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Payment Rest Api is running", response.getBody());
    }
}