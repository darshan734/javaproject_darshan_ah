package com.allstate.JavaProject.service;

import com.allstate.JavaProject.entities.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.text.SimpleDateFormat;
import java.util.Date;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.MockitoAnnotations.initMocks;

@SpringBootTest
class PaymentServiceImplementationTest {

    @Autowired
    private PaymentService paymentService;

    @Test
    public void save_And_findById_Success() {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Payment payment = new Payment(16, date, "Bank", 30000.0, 800);

        paymentService.save(payment);
        Payment paymentInfo = paymentService.findById(16);

        assertEquals(payment.getId(), paymentInfo.getId());

        String expected = dateFormatter.format(date);
        String actual = dateFormatter.format(paymentInfo.getPaymentDate());
        assertEquals(expected, actual);

        assertEquals(13, paymentService.findByType("Bank").size());
        assertEquals(14, paymentService.rowcount());
    }
}