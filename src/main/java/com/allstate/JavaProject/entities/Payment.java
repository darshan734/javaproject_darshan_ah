package com.allstate.JavaProject.entities;

import org.springframework.data.annotation.Id;
import java.util.Date;


public class Payment {

    @Id
    private int id;
    private Date paymentDate;
    private String type;
    private double amount;
    private int customerId;

    public Payment(int id, Date paymentDate, String type, double amount, int customerId) {
        this.id = id;
        this.paymentDate = paymentDate;
        this.type = type;
        this.amount = amount;
        this.customerId = customerId;
    }

    public Payment() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String toString() {
        return "The amount" + " " + this.getAmount() + " " + "Payment done on" + " " + this.getPaymentDate() +
                " " + " By customer Id" + " " + this.getCustomerId() + " " +"And Payment type is " +
                " " + this.getType();
    }

}
