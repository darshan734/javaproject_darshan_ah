package com.allstate.JavaProject;

import com.allstate.JavaProject.entities.Payment;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootApplication
public class JavaProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaProjectApplication.class, args);
		Date date = new Date();
		Payment payment = new Payment(1,date, "Bank",10000.0, 100);
		System.out.println(payment.toString());
	}
}
