package com.allstate.JavaProject.dao;

import com.allstate.JavaProject.entities.Payment;
import java.util.List;

public interface PaymentDAO {
    int rowcount();
    Payment findByID(int id);
    List<Payment> findByType(String type);
    int save(Payment payment);
    List<Payment> findAll();
}
