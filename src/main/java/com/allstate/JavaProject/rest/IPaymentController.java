package com.allstate.JavaProject.rest;

import com.allstate.JavaProject.entities.Payment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

public interface IPaymentController {
    @RequestMapping(value = "/apiStatus", method = RequestMethod.GET)
    String getStatus();

    @RequestMapping(value = "/rowcount", method = RequestMethod.GET)
    int rowcount();

    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    List<Payment> findAll();

    @RequestMapping(value = "/findbyid/{id}", method = RequestMethod.GET)
    ResponseEntity<Payment> findbyid(@PathVariable("id") int id);

    @RequestMapping(value = "/findbytype/{type}", method = RequestMethod.GET)
    ResponseEntity<List> findbytypeid(@PathVariable("type") String type);

    @RequestMapping(value = "/save" ,method = RequestMethod.POST)
    int savePayment(@RequestBody Payment payment);
}
