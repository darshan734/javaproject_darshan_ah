package com.allstate.JavaProject.rest;

import com.allstate.JavaProject.entities.Payment;
import com.allstate.JavaProject.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api/payments")
public class PaymentController implements IPaymentController {
    @Autowired
    private PaymentService paymentService;
    Logger logger = Logger.getLogger(PaymentController.class.getName());

    @Override
    @RequestMapping(value = "/apiStatus", method = RequestMethod.GET)
    public String getStatus()
    {
        logger.info("Payment status method");
        return "Payment Rest Api is running";
    }

    @Override
    @RequestMapping(value = "/rowcount", method = RequestMethod.GET)
    public int rowcount() {
        return paymentService.rowcount();
    }

    @Override
    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public List<Payment> findAll() {
        return paymentService.findAll();
    }

    @Override
    @RequestMapping(value = "/findbyid/{id}", method = RequestMethod.GET)
    public ResponseEntity<Payment> findbyid(@PathVariable("id") int id) {
        logger.info("findbyid method");
        Payment payment = paymentService.findById(id);

        if (payment == null){
            return new ResponseEntity<Payment>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Payment>(payment, HttpStatus.OK);
        }
    }

    @Override
    @RequestMapping(value = "/findbytype/{type}", method = RequestMethod.GET)
    public ResponseEntity<List> findbytypeid(@PathVariable("type") String type) {
        logger.info("findbytypeid method");
        List<Payment> paymentList= paymentService.findByType(type);

        if (paymentList.size() == 0){
             return new ResponseEntity<List>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<List>(paymentList,HttpStatus.OK);
        }
    }

    @Override
    @RequestMapping(value = "/save" ,method = RequestMethod.POST)
    public int savePayment(@RequestBody Payment payment) {
        logger.info("save method");
        return paymentService.save(payment);
    }

}
