package com.allstate.JavaProject.service;

import com.allstate.JavaProject.entities.Payment;

import java.util.List;

public interface PaymentService {
    int rowcount();
    List<Payment> findAll();
    Payment findById(int id);
    List<Payment> findByType(String type);
    int save(Payment payment);
}
