package com.allstate.JavaProject.service;

import com.allstate.JavaProject.service.PaymentService;
import com.allstate.JavaProject.dao.PaymentDAO;
import com.allstate.JavaProject.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PaymentServiceImplementation implements PaymentService {

    @Autowired
    private PaymentDAO dao;

    @Override
    public int rowcount() {
        return dao.rowcount();
    }

    @Override
    public List<Payment> findAll() {
        return dao.findAll();
    }

    @Override
    public Payment findById(int id) {

        return id > 0 ? dao.findByID(id) : null;
    }

    @Override
    public List<Payment> findByType(String type) {
        return type!=null & !type.trim().isEmpty() ? dao.findByType(type) : null;
    }

    @Override
    public int save(Payment payment) {
        return dao.save(payment);
    }
}
